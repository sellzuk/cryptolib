#pragma once
#define KEY_BIT_SIZE 128
#define MAC_LEN 32

#define SUCCESS 0						// 성공
#define FAIL -1
#define MSG_IS_NOT_VALID -2				// 메세지 해쉬가 상이함.

typedef unsigned char U8;