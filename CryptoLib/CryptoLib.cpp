﻿// CryptoLib.cpp : 정적 라이브러리를 위한 함수를 정의합니다.
//

#include "pch.h"
#include "framework.h"
#include "CryptoLib.h"
#include <array>
#include <random>

#include "base64.h"

#include <openssl/aes.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>

// TODO: 라이브러리 함수의 예제입니다.

static void PrintHex(char* str, int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%02x", (unsigned char)str[i]);
	}

	printf("\n");
}

int CCryptoLib::EncryptOpenSSL(U8* plain, U8** resEncrypt)
{
	AES_KEY aesKey;
	U8 key[AES_BLOCK_SIZE] = { 0, };
	U8 iv[AES_BLOCK_SIZE] = { 0, };
	U8 keyIv[AES_BLOCK_SIZE * 2] = { 0, };
	U8* mac = nullptr;
	U8* resCipher = nullptr;
	int totalSize = 0;
	int cipherLen = 0;
	int intSize = sizeof(int);
	int idx = 0;

	// key, iv 생성
	GenerateKey(key);
	GenerateKey(iv);

	// iv는 암호화 함수 호출 후 변경되기 때문에(round를 돌면서 xor하기 때문) 미리 key와 함께 복사해 놓는다
	memcpy(keyIv, key, AES_BLOCK_SIZE);
	memcpy(&keyIv[AES_BLOCK_SIZE], iv, AES_BLOCK_SIZE);

	// ase 암호화용 키 설정.
	AES_set_encrypt_key(key, KEY_BIT_SIZE, &aesKey);

	// 암호화할 평문의 길이와 암호화 된 이후의 길이 계산(cbc 이기 때문에 padding이 들어가서 항상 16의 배수가 된다).
	int plainLen = strlen((char*)plain);
	cipherLen = ((plainLen + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE;

	// 계산한 암호화된 길이에 따라 메모리 할당. 이유는 모르겠지만 new로 할당 시 오류가 발생되어 죽는 현상 발생.
	U8* cipher = (U8*)malloc(sizeof(U8) * cipherLen);

	// 메세지 검증을 할 hmac-sha256 hash 값을 생성(평문을 활용).
	mac = HMAC(EVP_sha256(), key, AES_BLOCK_SIZE, plain, plainLen, NULL, NULL);

	// call aes encrypt
	AES_cbc_encrypt(plain, cipher, plainLen, &aesKey, iv, AES_ENCRYPT);

	// 암호화문의 총사이즈는 cipher의 length와 key, iv, hmac, cipher의 길이 합이다.
	totalSize = intSize + (AES_BLOCK_SIZE * 2) + MAC_LEN + cipherLen + 1;
	
	// 계산한 사이즈 만큼 메모리 할당 후 초기화.
	resCipher = (U8*)malloc(totalSize);
	memset(resCipher, 0x00, totalSize);

	// cipher length, key, iv, hmac, cipher 순서대로 copy해준다.
	// cipher length
	memcpy(&resCipher[idx], &cipherLen, intSize);
	idx += intSize;

	// key, iv
	memcpy(&resCipher[idx], keyIv, AES_BLOCK_SIZE * 2);
	idx += (AES_BLOCK_SIZE * 2);

	// hmac
	memcpy(&resCipher[idx], mac, MAC_LEN);
	idx += MAC_LEN;

	// cipher
	memcpy(&resCipher[idx], cipher, cipherLen);
	idx += cipherLen;
	
	// 암호화된 문자열의 네트워크 통신 시 유실을 막기 위해 base64로 encoding.
	CBase64 base64;
	(*resEncrypt) = base64.Base64Encode(resCipher, totalSize);
	
	// 할당한 메모리 해제.
	free(resCipher);
	free(cipher);

	return totalSize;
}

int CCryptoLib::DecryptOpenSSL(U8* base64EncCipher, U8** plain)
{
	int result = SUCCESS;
	AES_KEY aesKey;
	U8 key[AES_BLOCK_SIZE + 1] = { 0, };
	U8 iv[AES_BLOCK_SIZE + 1] = { 0, };
	U8 mac[MAC_LEN + 1] = { 0, };
	U8* verifyMac = nullptr;
	U8* cipher = nullptr;
	U8* chBase64Dec = nullptr;
	int cipherLen = 0;
	int intSize = sizeof(int);
	int idx = 0;
	CBase64 base64;

	// base64 decoding을 위한 받은 base64 문자열의 length를 계산 후 decoding 된 문자열이 저장될 메모리 할당.
	int base64Len = base64.Base64DecodedSize(base64EncCipher);
	chBase64Dec = (U8*)malloc(sizeof(U8) * base64Len);

	// 할당된 메모리 초기화.
	memset(chBase64Dec, 0, base64Len);

	// base64 decoding.
	base64.Base64Decode(base64EncCipher, chBase64Dec, base64Len);

	// decoding된 문자열을 cipher length, key, iv, hmac, cipher 순서에 맞게 copy.
	// cipher length.
	memcpy(&cipherLen, &chBase64Dec[idx], intSize);
	idx += intSize;

	// key
	memcpy(key, &chBase64Dec[idx], AES_BLOCK_SIZE);
	idx += AES_BLOCK_SIZE;

	// iv
	memcpy(iv, &chBase64Dec[idx], AES_BLOCK_SIZE);
	idx += AES_BLOCK_SIZE;

	// hmac
	memcpy(mac, &chBase64Dec[idx], MAC_LEN);
	idx += MAC_LEN;

	// cipher.
	// cipher의 크기는 length를 가져온 후에 알 수 있기 때문에 메모리를 여기서 할당 해 준다.
	cipher = (U8*)malloc(sizeof(U8) * cipherLen + 1);
	memcpy(cipher, &chBase64Dec[idx], cipherLen + 1);

	// plain의 크기는 언제나 cipher의 크기보다 작기 때문에 cipher length 만큼 할당.
	(*plain) = (U8*)malloc(sizeof(U8) * cipherLen);

	// aes decrypt key 할당.
	AES_set_decrypt_key(key, KEY_BIT_SIZE, &aesKey);

	// aes decrypt
	AES_cbc_encrypt(cipher, (*plain), cipherLen, &aesKey, iv, AES_DECRYPT);

	// decrypt된 평문을 base로 검증용 hmac-sha256 hash를 생성.
	verifyMac = HMAC(EVP_sha256(), key, AES_BLOCK_SIZE, (*plain), strlen((char*)(*plain)), NULL, NULL);

	// 전달 받은 hmac과 평문 기반으로 받은 hmac을 비교하여 평문이 수정되지 않았는지 확인.
	if (strcmp((char*)mac, (char*)verifyMac) != 0)
	{
		result = FAIL;
	}

	// 사용이 끝난 메모리 해제.
	free(chBase64Dec);
	free(cipher);

	return result;
}

void CCryptoLib::GenerateKey(U8* key)
{
	// device random init
	std::random_device rd;

	std::mt19937 gen(rd());
	// 정규 분포를 사용
	std::uniform_int_distribution<int> rand(0, 255);

	for (int i = 0; i < AES_BLOCK_SIZE; i++)
	{
		key[i] = (U8)rand(gen);
	}
}