#include "pch.h"
#include "base64.h"

#include <windows.h>
#include <array>
#include <iostream>

static const char MimeBase64[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/'
};

static int DecodeMimeBase64[256] = {
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 00-0F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 10-1F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,  /* 20-2F */
	52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,  /* 30-3F */
	-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,  /* 40-4F */
	15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,  /* 50-5F */
	-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,  /* 60-6F */
	41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1,  /* 70-7F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 80-8F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 90-9F */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* A0-AF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* B0-BF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* C0-CF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* D0-DF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* E0-EF */
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1   /* F0-FF */
};

size_t b64_encoded_size(size_t inlen)
{
	size_t ret;

	ret = inlen;
	if (inlen % 3 != 0)
		ret += 3 - (inlen % 3);
	ret /= 3;
	ret *= 4;

	return ret;
}

size_t CBase64::Base64DecodedSize(const U8* in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen((char*)in);
	ret = len / 4 * 3;

	for (i = len; i-- > 0; ) {
		if (in[i] == '=') {
			ret--;
		}
		else {
			break;
		}
	}

	return ret;
}

/*
int CBase64::Base64Encode(char* text, int numBytes, char** encodedText)
{
	unsigned char input[3] = { 0,0,0 };
	unsigned char output[4] = { 0,0,0,0 };
	int   index, i, j, size;
	char* p, * plen;

	try
	{
		plen = text + numBytes - 1;
		size = (4 * (numBytes / 3)) + (numBytes % 3 ? 4 : 0) + 1;
		(*encodedText) = new char(size);
		j = 0;
		for (i = 0, p = text; p <= plen; i++, p++) {
			index = i % 3;
			input[index] = *p;
			if (index == 2 || p == plen) {
				output[0] = ((input[0] & 0xFC) >> 2);
				output[1] = ((input[0] & 0x3) << 4) | ((input[1] & 0xF0) >> 4);
				output[2] = ((input[1] & 0xF) << 2) | ((input[2] & 0xC0) >> 6);
				output[3] = (input[2] & 0x3F);
				(*encodedText)[j++] = MimeBase64[output[0]];
				(*encodedText)[j++] = MimeBase64[output[1]];
				(*encodedText)[j++] = index == 0 ? '=' : MimeBase64[output[2]];
				(*encodedText)[j++] = index < 2 ? '=' : MimeBase64[output[3]];
				input[0] = input[1] = input[2] = 0;
			}
		}
		(*encodedText)[j] = '\0';
	}
	catch (const std::exception ex)
	{
		throw ex;
	}

	return size;
}*/
U8* CBase64::Base64Encode(U8* in, size_t len)//, char** encodedText)
{
	U8* out;
	size_t  elen;
	size_t  i;
	size_t  j;
	size_t  v;

	if (in == NULL || len == 0)
		return NULL;

	elen = b64_encoded_size(len);
	out = (U8*)malloc(elen + 1);
	memset(out, 0x00, elen + 1);
	out[elen] = '\0';

	for (i = 0, j = 0; i < len; i += 3, j += 4) {
		v = in[i];
		v = i + 1 < len ? v << 8 | in[i + 1] : v << 8;
		v = i + 2 < len ? v << 8 | in[i + 2] : v << 8;

		out[j] = MimeBase64[(v >> 18) & 0x3F];
		out[j + 1] = MimeBase64[(v >> 12) & 0x3F];
		if (i + 1 < len) {
			out[j + 2] = MimeBase64[(v >> 6) & 0x3F];
		}
		else {
			out[j + 2] = '=';
		}
		if (i + 2 < len) {
			out[j + 3] = MimeBase64[v & 0x3F];
		}
		else {
			out[j + 3] = '=';
		}
	}

	return out;
}
/*
int CBase64::Base64Decode(char* text, unsigned char* dst, int numBytes)
{
	const char* cp;
	int space_idx = 0, phase;
	int d, prev_d = 0;
	unsigned char c;
	space_idx = 0;
	phase = 0;

	try
	{
		for (cp = text; *cp != '\0'; ++cp) {
			d = DecodeMimeBase64[(int)*cp];
			if (d != -1) {
				switch (phase) {
				case 0:
					++phase;
					break;
				case 1:
					c = ((prev_d << 2) | ((d & 0x30) >> 4));
					if (space_idx < numBytes)
						dst[space_idx++] = c;
					++phase;
					break;
				case 2:
					c = (((prev_d & 0xf) << 4) | ((d & 0x3c) >> 2));
					if (space_idx < numBytes)
						dst[space_idx++] = c;
					++phase;
					break;
				case 3:
					c = (((prev_d & 0x03) << 6) | d);
					if (space_idx < numBytes)
						dst[space_idx++] = c;
					phase = 0;
					break;
				}
				prev_d = d;
			}
		}
	}
	catch (const std::exception ex)
	{
		throw ex;
	}

	return space_idx;
}
*/

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int CBase64::Base64Decode(U8* in, U8* out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen((char*)in);
	if (outlen < Base64DecodedSize(in) || len % 4 != 0)
		return 0;

	for (i = 0; i < len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i = 0, j = 0; i < len; i += 4, j += 3) {
		v = b64invs[in[i] - 43];
		v = (v << 6) | b64invs[in[i + 1] - 43];
		v = in[i + 2] == '=' ? v << 6 : (v << 6) | b64invs[in[i + 2] - 43];
		v = in[i + 3] == '=' ? v << 6 : (v << 6) | b64invs[in[i + 3] - 43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i + 2] != '=')
			out[j + 1] = (v >> 8) & 0xFF;
		if (in[i + 3] != '=')
			out[j + 2] = v & 0xFF;
	}

	return 1;
}
