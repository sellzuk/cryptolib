#pragma once
#include "Defines.h"
#include "string.h"
#include <iostream>
#include <iomanip>

//namespace Serializer
//{
class CCryptoLib
{
public:
	static int EncryptOpenSSL(U8* plain, U8** resEncrypt);
	static int DecryptOpenSSL(U8* plain, U8** resEncrypt);

private:
	static void GenerateKey(U8* key);
};
//}