#pragma once
#include "Defines.h"

class CBase64
{
public:
	//static int Base64Encode(char* text, int numBytes, char** encodedText);
	U8* Base64Encode(U8* text, size_t numBytes);
	//static int Base64Decode(char* text, unsigned char* dst, int numBytes);
	int Base64Decode(U8* in, U8* out, size_t outlen);
	size_t Base64DecodedSize(const U8* in);
};